package it.com.atlassian.plugins.example.javaconfig;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.net.URI;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration test of the {@code greeter} module type.
 */
public class GreetingTest {

    /**
     * This test makes a request to the "greeting" endpoint, to retrieve the greetings for a test user "Mark". The
     * {@link com.atlassian.plugins.example.javaconfig.moduletype.Greeter} instances are defined by {@code <greeter>}
     * elements in {@code atlassian-plugin.xml}. This test is ultimately verifying that the OSGi Java config library is
     * correctly registering the {@link com.atlassian.plugins.example.javaconfig.moduletype.GreeterModuleDescriptorFactory}
     * with the OSGi service registry as a {@link com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory}.
     */
    @Test
    public void greetingEndpointShouldReturnTheGreetingsOfTheRegisteredGreeterPluginModules() throws Exception {
        // Arrange
        final HttpClient httpClient = HttpClientBuilder.create().build();
        final HttpResponse response = httpClient.execute(getRequestAsAdmin(greetingUriForTestUser()));

        // Act
        final String greeting = getBodyAsString(response);

        // Assert
        assertThat(greeting, equalTo("Hi Mark!,Bye Mark!"));
    }

    private HttpGet getRequestAsAdmin(final URI uri) throws Exception {
        final Credentials credentials = new UsernamePasswordCredentials("admin", "admin");
        final HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader(new BasicScheme().authenticate(credentials, httpGet, null));
        return httpGet;
    }

    private static URI greetingUriForTestUser() {
        // This URL is handled by the com.atlassian.plugins.example.javaconfig.web.GreetingResource
        return URI.create("http://localhost:5990/refapp/rest/spring-demo/greet/person?name=Mark");
    }

    private static String getBodyAsString(final HttpResponse response) throws Exception {
        assertThat(response.getStatusLine().getStatusCode(), is(HTTP_OK));
        return IOUtils.toString(response.getEntity().getContent(), UTF_8);
    }
}

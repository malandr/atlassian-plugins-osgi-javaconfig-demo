package com.atlassian.plugins.example.javaconfig.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import javax.annotation.ParametersAreNonnullByDefault;

import static java.lang.String.format;

/**
 * A {@link Condition} that checks the current environment (see subclasses for the supported environments).
 */
@ParametersAreNonnullByDefault
public abstract class Environment implements Condition {

    /**
     * The system property whose value is the active environment.
     */
    public static final String ENVIRONMENT_PROPERTY = "atlas.env";

    private final String name;

    protected Environment(final String name) {
        this.name = name;
    }

    @Override
    public final boolean matches(final ConditionContext context, final AnnotatedTypeMetadata metadata) {
        final String currentEnv = System.getProperty(ENVIRONMENT_PROPERTY);
        return name.equals(currentEnv);
    }
}

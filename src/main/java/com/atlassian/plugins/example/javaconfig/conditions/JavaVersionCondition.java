package com.atlassian.plugins.example.javaconfig.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Optional;

/**
 * A Spring bean {@link Condition} that checks whether the current Java version matches the
 * one specified by the {@link RequiresJava} annotation on the bean declaration. The
 * versions match if the runtime version number starts with the requested version number.
 */
public class JavaVersionCondition implements Condition {

    @Override
    public boolean matches(final ConditionContext context, final AnnotatedTypeMetadata metadata) {
        final String currentJavaVersion = System.getProperty("java.version");
        return Optional.ofNullable(metadata.getAnnotationAttributes(RequiresJava.class.getName()))
                .map(map -> map.get("value"))
                .map(Object::toString)
                .map(currentJavaVersion::startsWith)
                .orElse(false);
    }
}

package com.atlassian.plugins.example.javaconfig.conditions;

public class Prod extends Environment {

    public static final String ENVIRONMENT = "prod";

    public Prod() {
        super(ENVIRONMENT);
    }
}

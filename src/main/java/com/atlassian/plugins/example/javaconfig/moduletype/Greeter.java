package com.atlassian.plugins.example.javaconfig.moduletype;

import javax.annotation.Nonnull;

/**
 * Generates greeting messages.
 */
public interface Greeter {

    /**
     * Generates a personalised greeting.
     *
     * @param person the name of the person to greet
     * @return the greeting
     */
    @Nonnull
    String greet(String person);
}

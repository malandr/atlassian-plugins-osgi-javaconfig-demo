package com.atlassian.plugins.example.javaconfig.moduletype;

import javax.annotation.Nonnull;
import java.util.StringJoiner;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * A greeter that works by using {@link String#format}, where the person's name is the only argument.
 */
class StringFormatGreeter implements Greeter {

    private final String greetingFormat;

    StringFormatGreeter(final String greetingFormat) {
        this.greetingFormat = requireNonNull(greetingFormat);
    }

    @Nonnull
    @Override
    public String greet(final String person) {
        return format(greetingFormat, person);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", StringFormatGreeter.class.getSimpleName() + "[", "]")
                .add("greetingFormat='" + greetingFormat + "'")
                .toString();
    }
}
